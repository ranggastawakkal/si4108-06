Metode Pengembangan Sistem

	Metode yang digunakan dalam pengembangan ini adalah metode Waterfall. 
Metode Pengembangan Waterfall adalah metode pengembangan sistem perangkat lunak yang digunakan untuk menghasilkan produk tertentu, 
dan menguji keefektifan produk tersebut dengan spesifikasi yang tidak berubah-ubah.

	Menurut Nieveen (1999 : 127), kualitas produk dalam pendidikan, dalam
penelitian ini adalah perangkat pembelajaran yang dikembangkan, dapat dilihat dari
tiga aspek, yaitu kevalidan, kepraktisan, dan keefektifan. Jenis produk yang dihasilkan dalam pengembangan ini adalah aplikasi 
penyedia layanan jasa perjalanan bernama "HappyNewYear". 

	Produk yang dihasilkan ini akan diuji kelayakannya terlebih
dahulu. Untuk menguji layak atau tidaknya, awalnya aplikasi ini akan divalidasi
terlebih dahulu untuk melihat kevalidan dan kepraktisannya apabila digunakan
sebagaimana mestinya. Setelah aplikasi dikatakan valid, aplikasi ini akan digunakan oleh perusahaan
untuk melihat keefektifannya. Aplikasi yang sudah terbukti valid, praktis, dan
efektif dapat dikatakan layak untuk digunakan sebagai aplikasi layanan jasa perjalanan.
